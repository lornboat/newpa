
#include "sys.h"
#include "delay.h"  
#include "usart.h" 
#include "malloc.h"
#include "can.h"
#include "parameter.h"
#include "io.h"
#include "servosys.h"
#include "cansys.h"
#include "debug.h"
#include "step.h"
#define KEYUP 				PFin(9) 
#define KEYDOWN				PFin(10) 
#define KEYSUSPEND		PCin(1) 
#define SUSPENDSTATE	PEout(9)
extern struct parameter m_para;
extern struct servo_state m_state;
int main(void)
{
	char up_key;
	char down_key;
	char suspend_key;
	Stm32_Clock_Init(336,8,2,7);//????,168Mhz 
	delay_init(168);			//?????  
	uart_init(84,115200);		//?????????115200
	InitParameter();
	cansysinital();
	InitServoSystem();
	RCC->AHB1ENR|=1<<5;     //??PORTF??
	RCC->AHB1ENR|=1<<2;   //PORTC
	RCC->AHB1ENR|=1<<4;   //PORTC
	GPIO_Set(GPIOF,PIN9|PIN10,GPIO_MODE_IN,0,0,GPIO_PUPD_PU);	//P|F9~10??????
	GPIO_Set(GPIOC,PIN1,GPIO_MODE_IN,0,0,GPIO_PUPD_PU);
	GPIO_Set(GPIOE,PIN9,GPIO_MODE_OUT,GPIO_OTYPE_PP,GPIO_SPEED_100M,GPIO_PUPD_PU);
	#if isdebug
	printf("init ok\r\n");
	#endif
	up_key=0;
	down_key=0;
	suspend_key=0;
	SUSPENDSTATE=0;
	while(1)
	{
	//	CanReceive();
		CanCommandanalysis();
		if(KEYUP==0)
		{
			delay_us(20);
			if(KEYUP==0)
			{				
				if(up_key==0)
				{
					up_key=1;
					#if isdebug
						printf("speed=%d;drop_number=%d dir=1\r\n",m_para.m_step[1].speedval,m_para.m_step[1].step_drop_number);
					#endif
					StarStep(1,m_para.m_step[1].speedval*m_para.m_step[1].step_speed_quotiety,0xffffffff,m_para.m_step[1].step_drop_number*m_para.m_step[1].step_scaling,0,DISTANCE_TYPE);
				}
			}
		}
		else
		{
			delay_us(20);
			if(KEYUP==1)
			{
				if(up_key==1)
				{
					up_key=0;
					StopStep(1);
				}
			}
		}
		if(KEYDOWN==0)
		{
			delay_us(20);
			if(KEYDOWN==0)
			{				
				if(down_key==0)
				{
					down_key=1;
					#if isdebug
						printf("speed=%d;drop_number=%d dir=0\r\n",m_para.m_step[1].speedval,m_para.m_step[1].step_drop_number);
					#endif
					StarStep(1,m_para.m_step[1].speedval*m_para.m_step[1].step_speed_quotiety,0xffffffff,m_para.m_step[1].step_drop_number*m_para.m_step[1].step_scaling,1,DISTANCE_TYPE);
				}
			}
		}
		else
		{
			delay_us(20);
			if(KEYDOWN==1)
			{
				if(down_key==1)
				{
					down_key=0;
					StopStep(1);
				}
			}
		}
		if(KEYSUSPEND==0)
		{
			delay_us(200);
			if((KEYSUSPEND==0)&&(suspend_key==0))
			{
				if(m_state.suspendstate==0)
				{
					m_state.suspendstate=1;
					SUSPENDSTATE=1;
					delay_us(20);
				}
				else
				{
					m_state.suspendstate=0;
					SUSPENDSTATE=0;
					delay_us(20);
				}
				suspend_key=1;
			}
		}
		else
		{
			suspend_key=0;
		}
		
	}
	return 0;
}
