
#include "sys.h"
#include "parameter.h"
#include <string.h>
#include "usart.h"
#include "stmflash.h"
#include "debug.h"
#include "can.h"
#include "io.h"
#include "malloc.h"
#define	FLASH_PARA_SAVE_ADDR				ADDR_FLASH_SECTOR_11 


struct parameter m_para;
struct servo_state m_state;
/*
void mymemcpy(char* des,char* sou,int len)
{
	int i;
	for(i=0;i<len;i++)
	{
		des[i]=sou[i];
	}
}
void mymemset(char* des,char setval,int len)
{
	int i;
	char* p;
	p=des;
	for(i=0;i<len;i++)
	{
		*p=setval;
		p++;
	}
}*/
u8 ReadParameter(void)
{
	u8 ret=0;
	
		u8 ptemp[1024];

	
		int len=4+sizeof(struct parameter);
		
	
		STMFLASH_Read(FLASH_PARA_SAVE_ADDR,(u32*)ptemp,len);
	
		if((ptemp[0]=='P')&&(ptemp[1]=='A')&&(ptemp[len-2]=='w')&&(ptemp[len-1]=='j'))
		{
			
			mymemcpy((char*)&m_para,(char*)&ptemp[2],len-4);
			if(m_para.endsign!=0x7284) return 0;
			ret=1;
		}
	
	
	
	return ret;
}
int WriteParameter(void)
{
	
	u8 ptemp[1024];
	int len=4+sizeof(struct parameter);
		

	

	ptemp[0]='P';
	ptemp[1]='A';
	ptemp[len-2]='w';
	ptemp[len-1]='j';
	m_para.endsign=0x7284;	
	mymemcpy((char*)&ptemp[2],(char*)&m_para,len-4);
	
		
  STMFLASH_Write(FLASH_PARA_SAVE_ADDR,(u32*)ptemp,len);
		
	
		#if isdebug
		printf("write parameter\r\n");
		#endif
		
	
	
	return 0;
}

void InitParameter(void)
{
	
	if(ReadParameter()==0)
	{
	
		m_para.ip=my_can_id;
		m_para.mainip=main_can_id;
		
		#if isdebug
		printf("read parameter error\r\n");
		#endif
		WriteParameter();
	}
	
	#if isdebug
	else
	{
		printf("read parameter ok\r\n");
		
	}
	 printf("can ip is 0x%x\r\n",m_para.ip);
	#endif
	memset(&m_state,0,sizeof(struct servo_state));
//	m_state.litfer_difference=0;
//	m_para.ip=my_can_id;
	
}
unsigned char GetBoardIP(void)
{
	return (unsigned char)m_para.ip;
}
void Get_Board_State(void)
{

				
	


}
void SetYRasterScaling(unsigned char* buf,int len)
{
	memcpy(&m_para.y_raster_scaling,&buf[2],sizeof(unsigned int));
}


void SetServoPara(unsigned char* buf,int len)
{
	unsigned short servono;
	
	if(len<sizeof(struct steppara)+4)
	{
		return;
	}
	memcpy(&servono,&buf[2],sizeof(unsigned short));
	
	memcpy(&m_para.m_step[servono],&buf[4],sizeof(struct steppara));
	#if isdebug
	 printf("servopara servono=%d,speed=%d\r\n",servono,m_para.m_step[servono].speedval);
	#endif
	WriteParameter();
}
void GetServoPara(unsigned char* buf,int len)
{
	unsigned short servono;
	memcpy(&servono,&buf[2],sizeof(unsigned short));
}
