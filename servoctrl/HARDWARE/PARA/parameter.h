#ifndef __PARAMETER_H
#define __PARAMETER_H

#include "sys.h"
struct steppara
{
	unsigned short step_drop_number;//
	unsigned short low_step_drop_number;//
	unsigned int step_scaling;//
	unsigned int step_speed_quotiety;//
	unsigned short directional_logic[2];
	unsigned short speedval;//
	unsigned short lowspeed;//
	
	
};
struct parameter
{
	struct steppara m_step[4];
	unsigned int ip;
	unsigned int mainip;
	unsigned int step_o_timer;//零速反应时间
	unsigned int litfer_seal_p;
	unsigned int litfer_print_p;
	unsigned int sealer_seal_p;

	unsigned char step_limit_dir[2];
	unsigned int y_raster_scaling;//1/1000
	unsigned int endsign;
};
struct servo_state
{
	unsigned char stepstate[3];
	unsigned char step_limit_p[3];
	unsigned char step_limit_n[3];
	unsigned char step_warning[3];
	unsigned int suspendstate;
	
};
unsigned char ReadParameter(void);
int WriteParameter(void);
void InitParameter(void);
unsigned char GetBoardIP(void);
void Get_Board_State(void);
void SetServoPara(unsigned char* buf,int len);
void SetYRasterScaling(unsigned char* buf,int len);
//void mymemcpy(char* des,char* sou,int len);
//void mymemset(char* des,char setval,int len);
#endif
