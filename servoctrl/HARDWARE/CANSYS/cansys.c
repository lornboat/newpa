#include "cansys.h"
#include "parameter.h"
#include "can.h"
#include "delay.h"
#include "sys.h"
#include "malloc.h"
#include <string.h>
#include "servosys.h"
#include "usart.h"
#include "debug.h"

extern struct parameter m_para;
extern struct servo_state m_state;
extern unsigned int y_raster_val;
extern unsigned int next_y_pl;
unsigned char revbuf[buf_number][buflen];
char commandbufs[commandnu][commandbufsize];
int revlen[buf_number];
unsigned char* pwrite;
int curwritebuf;
int curreadbuf;
unsigned char* pread;
struct cancommand m_command[commandnu];
struct cancommand *p_write_command;
struct cancommand *p_read_command;
char analysiscommand(unsigned char* buf,int len);
void cansysinital(void)
{
	int i,j;
	CAN_Mode_Init(CAN1,1,6,7,6,0);	//CAN初始化,波特率500Kbps  普通模式
	for(j=0;j<buf_number;j++)
	{
		for(i=0;i<buf_number;i++)
		{
			revbuf[j][i]=0;
		}
		revlen[j]=0;
	}
	pwrite=revbuf[0];
	pread=revbuf[0];
	curwritebuf=0;
	curreadbuf=0;
	for(i=0;i<commandnu;i++)
	{
		if(i==commandnu-1)
		{
			m_command[i].next=&m_command[0];
		}
		else
		{
			m_command[i].next=&m_command[i+1];
		}
		m_command[i].commandbuf=commandbufs[i];
		m_command[i].commandlen=0;
		m_command[i].state=0;
	}
	p_write_command=&m_command[0];
	p_read_command=&m_command[0];
}
void SendCommand(unsigned char *buf,int len)
{
	int i;
	short sendip=(short)m_para.mainip;
	unsigned char can_buf[8];
//	unsigned char ktemp=0;
	for(i=0;i<8;i++)
	{
		can_buf[i]=0;
	}
	can_buf[0]=main_can_id;
	memcpy(&can_buf[0],&sendip,2);
	for(i=0;i<len;i++)
	{
		can_buf[i+2]=buf[i];
	}
	
	CAN_Send_Msg(CAN1,can_buf,8);
}
void SendData(unsigned char* buf,int len)
{
	int willsendlen;
	unsigned char* pdata;
	pdata=buf;
	willsendlen=len;
	while(willsendlen)
	{
		int sendedlen;
		if(willsendlen>=persendlen)
		{
			sendedlen=persendlen;
		}
		else
		{
			sendedlen=willsendlen;
		}
		SendCommand(pdata,sendedlen);
	
//		printf(".");
		pdata+=sendedlen;
		willsendlen-=sendedlen;
		delay_ms(8);
	}
//	printf("\r\n");
}
void SendOneCommand(unsigned char* buf,int len,unsigned char command)
{
	int sendlen;
	unsigned short srlen;
	unsigned char sendbuf[1044];
	srlen=(unsigned short)len;
	sendlen=len+6;
//	sendbuf=(unsigned char*)my_mem_malloc(SRAMIN,(unsigned int)sendlen);
	if(sendbuf==NULL)
	{
		#if isdebug
		printf("malloc mem error\r\n");
		#endif
		return;
	}
	mymemset(sendbuf,0,sendlen);
	sendbuf[0]='l';
	sendbuf[1]='j';
	sendbuf[2]='z';
	sendbuf[3]=command;
	mymemcpy(&sendbuf[4],&srlen,sizeof(unsigned short));
	mymemcpy(&sendbuf[6],buf,len);
	srlen=0;
//	for(i=0;i<sendlen-2;i++)
//	{
//		srlen+=sendbuf[i];
//	}
//	mymemcpy(&sendbuf[sendlen-2],&srlen,sizeof(unsigned short));
	#if isdebug
//	{
//		printf("sendlen=%d\r\n",sendlen);
//		for(i=0;i<sendlen;i++)
//		{
//			printf("0x%x;",sendbuf[i]);
	//	}
//	}
	#endif
	SendData(sendbuf,sendlen);
//	myfree(SRAMIN,sendbuf);
}
void SendParameter(void)
{
	int len;
	unsigned char* pdata;
	
	pdata=(unsigned char*)&m_para;
	
	len=sizeof(struct parameter);
	
	SendOneCommand(pdata,len,parameter_command);
}
void Sendack(char rt)
{
	SendOneCommand((unsigned char*)&rt,1,ack_er);
	#if isdebug
//	printf(">");
	#endif
}
void SendState(void)
{
	
	int len;
	unsigned char* pdata;
	
	pdata=(unsigned char*)&m_state;
	
	len=sizeof(struct servo_state);
	#if isdebug
	printf("send state len = %d\r\n",len);
	#endif
	SendOneCommand(pdata,len,state_command);
}
void SendServoPara(unsigned short no)
{
	int len;
	unsigned char pdata[500];
	memcpy(&pdata[0],&no,sizeof(unsigned short));
//	pdata=(unsigned char*)&m_para.m_step[no];
	memcpy(&pdata[2],&m_para.m_step[no],sizeof(struct steppara));
	len=sizeof(struct steppara)+2;
	#if isdebug
	printf("send Servo para no = %d\r\n",no);
	#endif
	SendOneCommand(pdata,len,get_servo_parameter);
}
void add_buf(int* numb)
{
	if(*numb==(buf_number-1))
	{
		*numb=0;
	}
	else
	{
		*numb=(*numb)++;
	}
}
void CanReceiveMain(unsigned char* buf)
{
	int i;
	#if isdebug
/*	for(i=0;i<8;i++)
	{
		printf("0x%x;",buf[i]);
	}
	printf("\r\n");*/
	#endif
	if(revlen[curwritebuf]>1400)
	{
		revlen[curwritebuf]=0;
		pwrite=revbuf[curwritebuf];
	}
	for(i=0;i<persendlen;i++)
	{
		*pwrite=buf[i+2];
		pwrite++;
		revlen[curwritebuf]++;
	}
	if(revlen[curwritebuf]>=6)
	{
		#if isdebug
	//		printf("is one rev len = %d\r\n",revlen[curwritebuf]);
		#endif
		for(i=0;i<revlen[curwritebuf]-5;i++)
		{
			if((pread[i]=='l') || (pread[i+1]=='j') || (pread[i+2]== 'z'))
			{
				unsigned short len;
				mymemcpy(&len,&revbuf[curwritebuf][i+4],sizeof(unsigned short));
				if((len<1440)&&(revlen[curwritebuf]>=len+i))
				{
			
					#if isdebug
						printf("is one command len = %d; i=%d\r\n",len,i);
					#endif
	//				analysiscommand(&pread[i+3],len-3);
					memcpy(p_write_command->commandbuf,&pread[i+3],len-3);
					p_write_command->commandlen=len-3;
					p_write_command->state=1;
					p_write_command=p_write_command->next;
					add_buf(&curwritebuf);
					pwrite=revbuf[curwritebuf];
					revlen[curreadbuf]=0;
					add_buf(&curreadbuf);
					pread=revbuf[curreadbuf];
					break;
				}
				else
				{
					if(len>1440)
					{
						pwrite=revbuf[curwritebuf];
						revlen[curwritebuf]=0;
					}
				}
			}				
		}
		
			
		
	}
}
void SendServoStopState(unsigned char no)
{
	SendOneCommand((unsigned char*)&no,1,servo_is_stop);
}
void CanReceive(void)
{
	if(curwritebuf!=curreadbuf)
	{
		int j;
		for(j=0;j<revlen[curreadbuf]-5;j++)
		{
			if((pread[j]=='l') || (pread[j+1]=='j') || (pread[j+2]== 'z')) 
			{
				unsigned short len;
				mymemcpy(&len,&pread[4],sizeof(unsigned short));
				if(len<=1024)
				{
					int i;
					int templen;
					unsigned short te;
					unsigned short temp=0;
					templen=len + 8;
					for(i=0;i<templen-2;i++)
					{
						temp+=pread[i+j];
					}
					mymemcpy(&te,&pread[j+templen-2],sizeof(unsigned short));
					if(temp==te)
					{
						#if isdebug
						printf("one command\r\n");
						#endif
						analysiscommand(&pread[j+3],templen-5);
						break;
					}
				}
			}
		}
		revlen[curreadbuf]=0;
		add_buf(&curreadbuf);
		pread=revbuf[curreadbuf];
		
	}
}
void SendYPl(void)
{
	unsigned char* pdata;
	pdata=(unsigned char*)&y_raster_val;
	SendOneCommand(pdata,sizeof(unsigned int),get_y_pl);
}
void SendYNextPl(void)
{
	unsigned char* pdata;
	pdata=(unsigned char*)&next_y_pl;
	SendOneCommand(pdata,sizeof(unsigned int),get_y_next_pl);
}
void SendYRasterScaling(void)
{
	unsigned char* pdata;
	pdata=(unsigned char*)&m_para.y_raster_scaling;
	SendOneCommand(pdata,sizeof(unsigned int),get_y_raster_scaling);
}
char analysiscommand(unsigned char* buf,int len)
{
	char kret = 1;
	char ret = 0;
	#if isdebug
						printf("buf[0]=%d; \r\n",buf[0]);
	#endif
	switch(buf[0])
	{
		case parameter_command:
		
			if((len-3)==sizeof(struct parameter))
			{
				mymemcpy(&m_para,&buf[3],sizeof(struct parameter));
				WriteParameter();
			}
		
			break;
		case request_state:
			 SendState();
			 return 1;
				break;
		case state_command:
		
			break;
		case set_servo_parameter:
			SetServoPara(&buf[1],len-1);
			break;
		case run_servo:
			return ServoRun(&buf[1],len-3);
			
			break;
		case reset_y_pl:
			y_raster_val=0;
			break;
		case get_y_pl:
			SendYPl();
			return 0;
			break;
		case get_y_next_pl:
			SendYNextPl();
			return 0;
			break;
		case get_y_raster_scaling:
			SendYRasterScaling();
			return 0;
			break;
		case set_y_raster_scaling:
			SetYRasterScaling(&buf[1],len-1);
			break;
		case get_servo_parameter:
		{
			unsigned short servotype;
			memcpy(&servotype,&buf[3],sizeof(unsigned short));
			#if isdebug
			printf("send Servo para no = %d\r\n",servotype);
			#endif
			if((servotype>=0)&&(servotype<4))
			{
				SendServoPara(servotype);
				return 1;
			}
		}
			break;
	
	}
	
		Sendack(ret);
	return kret;
}
void CanCommandanalysis(void)
{
	if(p_read_command->state==1)
	{
		if(analysiscommand((unsigned char*)p_read_command->commandbuf,p_read_command->commandlen))
		{
			p_read_command->state=0;
			p_read_command=p_read_command->next;
		}
	}
}
void SendTest(void)
{
	unsigned char buf[8];
	int ki;
	for(ki=0;ki<8;ki++)
	{
		buf[ki]=(unsigned char)(ki+1);
	}
	CAN_Send_Msg(CAN1,buf,8);
}

