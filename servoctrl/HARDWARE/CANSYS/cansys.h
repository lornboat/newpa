#ifndef __CANSYS_H
#define __CANSYS_H


#define	parameter_command							1   
#define state_command									2
#define	request_parameter							3
#define request_state									4
#define	set_servo_parameter						5
#define run_servo											6
#define servo_is_stop									7
#define reset_y_pl										8
#define get_y_pl											9
#define get_servo_parameter						10
#define get_y_next_pls								11
#define get_y_raster_scaling					12
#define set_y_raster_scaling					13

#define ack_er												22
#define setanglesensor_o							25

#define buf_number										2
#define buflen												1440
#define commandnu											10
#define commandbufsize								256

#define persendlen										6
struct cancommand
{
	char state;
	char *commandbuf;
	short commandlen;
	short sendip;
	struct cancommand *next;
};
void CanReceiveMain(unsigned char* buf);
void cansysinital(void);
void CanReceive(void);
void SendState(void);
void Sendack(char rt);
void SendTest(void);
void SendServoStopState(unsigned char no);
void CanCommandanalysis(void);

#endif
