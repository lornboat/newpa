#ifndef __SERVOSYS_H
#define __SERVOSYS_H
#define lift_left							1
#define lift_right						2
#define sealcar								3
#define nouse									-1
void InitServoSystem(void);

void GetServoStateNow(void);
void Stopall(void);
char ServoRun(unsigned char* buf,int len);
#endif 
