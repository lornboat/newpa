#include "servosys.h"
#include "step.h"
#include <string.h>

#include "malloc.h"
#include "parameter.h"
#include "debug.h"
#include "usart.h"
#include "delay.h"
extern struct servo_state m_state;
extern int errornumber;
char automode;
void InitServoSystem(void)
{
	InitStep();
	
	automode=0;
	ResetStopState();
}


void GetServoStateNow(void)
{
	for(int i=0;i<4;i++)
	{
		m_state.stepstate[i]=GetServoState((unsigned char)i);
		m_state.step_limit_p[i]=GetPLimitState((unsigned char)i);
		m_state.step_limit_n[i]=GetNLimitState((unsigned char)i);
	}
}
void Stopall()
{
}
char ServoRun(unsigned char* buf,int len)
{
	unsigned int distance;
	unsigned short speed;
	unsigned short servodir;
	unsigned short dropnum;
	unsigned short servono;
	unsigned short revlen;
	memcpy(&revlen,&buf[0],sizeof(unsigned short));
	if(len<revlen-6) return 0;
	memcpy(&distance,&buf[2],sizeof(unsigned int));
	memcpy((void*)&speed,(void*)&buf[6],sizeof(unsigned short));
	memcpy((void*)&servodir,(void*)&buf[8],sizeof(unsigned short));
	memcpy((void*)&dropnum,(void*)&buf[10],sizeof(unsigned short));
	memcpy((void*)&servono,(void*)&buf[12],sizeof(unsigned short));
	#if isdebug
	printf("distance=%d,speed=%d,servodir=%d,dropnum=%d,servono=%d\r\n",distance,speed,servodir,dropnum,servono);
	#endif
	return StarStep((u8)servono,speed,distance,dropnum,(u8)servodir,DISTANCE_TYPE);
}
