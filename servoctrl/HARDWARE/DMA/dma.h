#ifndef __DMA_H
#define	__DMA_H	   
#include "sys.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//DMA 驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/7
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved									  
////////////////////////////////////////////////////////////////////////////////// 	 
#define 		io_to_mem								0 
#define			mem_to_io								1
#define			mem_to_mem							2

void MYDMA_Config(DMA_Stream_TypeDef *DMA_Streamx,u8 chx,u8 mod,u32 par,u32 mar1,u32 mar2,u16 ndtr);//配置DMAx_CHx
void MYDMA_Enable(DMA_Stream_TypeDef *DMA_Streamx,u16 ndtr);	//使能一次DMA传输		  
u32 GetNDTR(DMA_Stream_TypeDef *DMA_Streamx);
u8 GetDMA_LISR(void);
u8 GetCT(DMA_Stream_TypeDef *DMA_Streamx);
#endif






























