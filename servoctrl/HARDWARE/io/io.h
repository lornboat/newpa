#ifndef __IO_H
#define __IO_H	
#include "sys.h" 

#define CCLK_FPGA(x) 						PBout((x+5))
#define DIN_FPGA(x)							PDout((x+2))
#define PROGRAM_FPGA(x) 				PCout((x+4))
#define INIT_READ(x)						PGin((x+8))
#define BOARD120V_STATE(x)			((x<2)?(PBin((x+14))):(PDin((x+8))))

#define PORT_NU									8
#define	FQ_A										PFin(0)
#define	FQ_B										PFin(1)
#define	Solenoid_A_Open()				PEout(8)=0
#define	Solenoid_B_Open()				PEout(9)=0
#define	Solenoid_in_Open()			PEout(10)=0
#define	Solenoid_A_Close()			PEout(8)=1
#define	Solenoid_B_Close()			PEout(9)=1
#define	Solenoid_in_Close()			PEout(10)=1
#define	Posi_Open()							PEout(11)=0
#define Posi_Close()						PEout(11)=1

void Init_IO(void);
void Beep_Sys_Start(void);
void Warn_Beep(void);
#endif 

