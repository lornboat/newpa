#include "timer.h"
#include "step.h"
#include "usart.h"
#include "delay.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32F407开发板
//定时器 驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2014/5/4
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2014-2024
//All rights reserved									  
////////////////////////////////////////////////////////////////////////////////// 	 
//定时器2中断服务程序
extern struct _m_step m_step[];
u8 firstenter;
void TIM2_IRQHandler(void)
{ 		    		  			    
	if(TIM2->SR&0X0001)//溢出中断
	{
	//	LED1=!LED1;	
	//	printf("#");
		StepHand(TIM2,0,(volatile unsigned long*)myaddr(GPIOB_ODR_Addr,6));
	}				   
	TIM2->SR&=~(1<<0);//清除中断标志位 	    
}
//通用定时器2中断初始化
//这里时钟选择为APB1的2倍，而APB1为42M
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器2!
void TIM2_Int_Init(u16 arr,u16 psc)
{
//	TIM2->SR&=~(1<<0);//清除中断标志位 	
	TIM2->CR1&=0xFE;//关闭定时器
	RCC->APB1ENR|=1<<0;	//TIM2时钟使能    
 	TIM2->ARR=arr;  	//设定计数器自动重装值 
	TIM2->PSC=psc;  	//预分频器	  
	TIM2->CR1|=0x01;    //使能定时器2
	TIM2->DIER|=1<<0;   //允许更新中断	  
	
	
//	delay_ms(2000);
//	printf("init timer\r\n");
//	printf("init timer\r\n");
  MY_NVIC_Init(0,2,TIM2_IRQn,2);	//抢占1，子优先级0，组2	
//	TIM2->DIER|=1<<0;   //允许更新中断	  
//	TIM2->CR1|=0x01;    //使能定时器2
	
	//printf("init timer2 arr=%d,psc=%d\r\n",arr,psc);
	TIM2->SR&=~(1<<0);//清除中断标志位 	    
}


//定时器3中断服务程序	 
void TIM3_IRQHandler(void)
{ 		    		  			    
	if(TIM3->SR&0X0001)//溢出中断
	{
	//	LED1=!LED1;
		
		StepHand(TIM3,1,(volatile unsigned long*)myaddr(GPIOB_ODR_Addr,7));
	}				   
	TIM3->SR&=~(1<<0);//清除中断标志位 	    
}
//通用定时器3中断初始化
//这里时钟选择为APB1的2倍，而APB1为42M
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器3!
void TIM3_Int_Init(u16 arr,u16 psc)
{
	RCC->APB1ENR|=1<<1;	//TIM3时钟使能    
 	TIM3->ARR=arr;  	//设定计数器自动重装值 
	TIM3->PSC=psc;  	//预分频器	  
	TIM3->DIER|=1<<0;   //允许更新中断	  
	TIM3->CR1|=0x01;    //使能定时器3
  MY_NVIC_Init(1,2,TIM3_IRQn,2);	//抢占1，子优先级1，组2	
	TIM3->SR&=~(1<<0);//清除中断标志位 	
}


//定时器4中断服务程序	 
void TIM4_IRQHandler(void)
{ 		    		  			    
	if(TIM4->SR&0X0001)//溢出中断
	{
	//	LED1=!LED1;	
		StepHand(TIM4,2,(volatile unsigned long*)myaddr(GPIOB_ODR_Addr,8));
	}				   
	TIM4->SR&=~(1<<0);//清除中断标志位 	    
}
//通用定时器4中断初始化
//这里时钟选择为APB1的2倍，而APB1为42M
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器4!
void TIM4_Int_Init(u16 arr,u16 psc)
{
	RCC->APB1ENR|=1<<2;	//TIM4时钟使能    
 	TIM4->ARR=arr;  	//设定计数器自动重装值 
	TIM4->PSC=psc;  	//预分频器	  
	TIM4->DIER|=1<<0;   //允许更新中断	  
	TIM4->CR1|=0x01;    //使能定时器4
  MY_NVIC_Init(2,2,TIM4_IRQn,2);	//抢占1，子优先级1，组2	
	TIM4->SR&=~(1<<0);//清除中断标志位 	    
}


//定时器5中断服务程序	 
void TIM5_IRQHandler(void)
{ 		    		  			    
	if(TIM5->SR&0X0001)//溢出中断
	{
	//	LED1=!LED1;	
		StepHand(TIM5,3,(volatile unsigned long*)myaddr(GPIOB_ODR_Addr,9));
	}				   
	TIM5->SR&=~(1<<0);//清除中断标志位 	    
}
//通用定时器5中断初始化
//这里时钟选择为APB1的2倍，而APB1为42M
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器5!
void TIM5_Int_Init(u16 arr,u16 psc)
{
	RCC->APB1ENR|=1<<3;	//TIM5时钟使能    
 	TIM5->ARR=arr;  	//设定计数器自动重装值 
	TIM5->PSC=psc;  	//预分频器	  
	TIM5->DIER|=1<<0;   //允许更新中断	  
	TIM5->CR1|=0x01;    //使能定时器5
  MY_NVIC_Init(3,2,TIM5_IRQn,2);	//抢占1，子优先级1，组2	
	TIM5->SR&=~(1<<0);//清除中断标志位 	    
}


//定时器9中断服务程序	 
void TIM1_BRK_TIM9_IRQHandler(void)
{ 		    		  			    
	if(TIM9->SR&0X0001)//溢出中断
	{
	//	LED1=!LED1;	
//		StepHand(TIM9,4,(volatile unsigned long*)myaddr(GPIOE_ODR_Addr,4));
		Step_O_outtime(TIM9,0);
	}				   
	TIM9->SR&=~(1<<0);//清除中断标志位 	    
}
//通用定时器9中断初始化
//这里时钟选择为APB1的2倍，而APB1为42M
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器9!
void TIM9_Int_Init(u16 arr,u16 psc)
{
	RCC->APB2ENR|=1<<16;	//TIM5时钟使能    
 	TIM9->ARR=arr;  	//设定计数器自动重装值 
	TIM9->PSC=psc;  	//预分频器	  
	TIM9->DIER|=1<<0;   //允许更新中断	  
	TIM9->CR1|=0x01;    //使能定时器9
  	MY_NVIC_Init(0,1,TIM1_BRK_TIM9_IRQn,2);	//抢占1，子优先级1，组2	
		TIM9->SR&=~(1<<0);//清除中断标志位 	 
}


//定时器10中断服务程序	 
void TIM1_UP_TIM10_IRQHandler(void)
{ 		    		  			    
	if(TIM10->SR&0X0001)//溢出中断
	{
	//	LED1=!LED1;	
//		StepHand(TIM10,5,(volatile unsigned long*)myaddr(GPIOE_ODR_Addr,5));
		Step_O_outtime(TIM10,1);
	}				   
	TIM10->SR&=~(1<<0);//清除中断标志位 	    
}
//通用定时器9中断初始化
//这里时钟选择为APB1的2倍，而APB1为42M
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器10!
void TIM10_Int_Init(u16 arr,u16 psc)
{
	RCC->APB2ENR|=1<<17;	//TIM5时钟使能    
 	TIM10->ARR=arr;  	//设定计数器自动重装值 
	TIM10->PSC=psc;  	//预分频器	  
	TIM10->DIER|=1<<0;   //允许更新中断	  
	TIM10->CR1|=0x01;    //使能定时器10
  	MY_NVIC_Init(1,1,TIM1_UP_TIM10_IRQn,2);	//抢占1，子优先级1，组2			
		TIM10->SR&=~(1<<0);//清除中断标志位 	    
}


//定时器11中断服务程序	 
void TIM1_TRG_COM_TIM11_IRQHandler(void)
{ 		    		  			    
	if(TIM11->SR&0X0001)//溢出中断
	{
	//	LED1=!LED1;		
//		StepHand(TIM11,6,(volatile unsigned long*)myaddr(GPIOE_ODR_Addr,6));
		Step_O_outtime(TIM11,2);
	}				   
	TIM11->SR&=~(1<<0);//清除中断标志位 	    
}
//通用定时器11中断初始化
//这里时钟选择为APB1的2倍，而APB1为42M
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器9!
void TIM11_Int_Init(u16 arr,u16 psc)
{
	RCC->APB2ENR|=1<<18;	//TIM5时钟使能    
 	TIM11->ARR=arr;  	//设定计数器自动重装值 
	TIM11->PSC=psc;  	//预分频器	  
	TIM11->DIER|=1<<0;   //允许更新中断	  
	TIM11->CR1|=0x01;    //使能定时器11
  	MY_NVIC_Init(2,1,TIM1_TRG_COM_TIM11_IRQn,2);	//抢占1，子优先级1，组2		
		TIM11->SR&=~(1<<0);//清除中断标志位 	    
}

//定时器12中断服务程序	 
void TIM8_BRK_TIM12_IRQHandler(void)
{ 		    		  			    
	if(TIM12->SR&0X0001)//溢出中断
	{
	//	LED1=!LED1;	
	//	StepHand(TIM12,7,(volatile unsigned long*)myaddr(GPIOE_ODR_Addr,7));
		Step_O_outtime(TIM12,3);
	}				   
	TIM12->SR&=~(1<<0);//清除中断标志位 	    
}
//通用定时器12中断初始化
//这里时钟选择为APB1的2倍，而APB1为42M
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=((arr+1)*(psc+1))/Ft us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器12!
void TIM12_Int_Init(u16 arr,u16 psc)
{
	RCC->APB1ENR|=1<<6;	//TIM5时钟使能    
 	TIM12->ARR=arr;  	//设定计数器自动重装值 
	TIM12->PSC=psc;  	//预分频器	  
	TIM12->DIER|=1<<0;   //允许更新中断	  
	TIM12->CR1|=0x01;    //使能定时器12
  	MY_NVIC_Init(3,1,TIM8_BRK_TIM12_IRQn,2);	//抢占1，子优先级1，组2		
		TIM12->SR&=~(1<<0);//清除中断标志位 	    
}
//定时器13中断服务程序
void TIM8_UP_TIM13_IRQHandler(void)
{
	if(TIM13->SR&0X0001)//溢出中断
	{
		if(firstenter==0)
		{
			firstenter=1;
		}
		else
		
		steptimeout_out();
//		printf("cnt is %d\r\n",TIM13->CNT);
	}
	TIM13->SR&=~(1<<0);//清除中断标志位 	 
}

void TIM13_Int_Init(u16 arr,u16 psc)
{
	RCC->APB1ENR|=1<<7;	//TIM13时钟使能    
	TIM13->CR1|=1<<1;
 	TIM13->ARR=arr;  	//设定计数器自动重装值 
	TIM13->PSC=psc;  	//预分频器	  
	TIM13->DIER|=1<<0;   //允许更新中断	  
	TIM13->CR1|=0x01;    //使能定时器13
	TIM13->CR1|=1<<2;
	TIM13->EGR|=0x01;
	firstenter=0;
	TIM13->SR&=~(1<<0);//清除中断标志位 	
	TIM13->CR1&=~(1<<1);	
  MY_NVIC_Init(3,2,TIM8_UP_TIM13_IRQn,2);	//抢占3，子优先级2，组2			
		TIM13->SR&=~(1<<0);//清除中断标志位 	 
}
void StopTIM13(void)
{
	TIM13->CR1&=0xFE;//关闭定时器
	TIM13->SR&=~(1<<0);//清除中断标志位 
}


