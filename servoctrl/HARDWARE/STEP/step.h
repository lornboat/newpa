#ifndef __STEP_H
#define __STEP_H

#include "sys.h"

//dir
#define		DIR_P					0 //正向
#define		DIR_N					1 //负向

#define		STOP_STATE				0 //停止状态
#define		ASCENDANT_STATE			1 //上升状态
#define		CONSTANT_STATE			2 //匀速状态
#define		DROP_STATE				3 //下降状态

//type
#define		DISTANCE_TYPE			0  //长度
#define		NO_DISTANCE_TYPE		1  //无限长度

//stoptype
#define		NORMAL_STOP				0  //正常停
#define		EMERGENCY_STOP			1  //急停

#define		STEP_PLUG(x)			PBout((x+6))

#define		STEP_DIR(x)				PAout((x+5))
#define		myaddr(addr,bitnum)		BITBAND(addr, bitnum)

#define	step_timer_psc							23

struct _m_step
{
	u8 type;
	u8 dir;
	u8 state;
	u8 stoptype;
	u16 curnumberplace;
	u16 dropnumber;
	u32 distance;
	u32 curdistance;
	u16 *dropdata;
	void (*inittimer)(u16 ,u16);
	void (*o_speed_timer)(u16,u16);
};
void InitStep(void);
void StepHand(TIM_TypeDef *timers,u8 no,volatile unsigned long*  place);
void StopStep(u8 no);
void SetStopType(u8 no,u8 stoptype);
u8 StepDir(u8 no);
void FreeDropData(u8 no);
u8 StarStep(u8 no,u16 speed,u32 distance,u16 dropnumber,u8 dir,u8 type);
u8 GetNLimitState(u8 no);
u8 GetPLimitState(u8 no);
void LimitSeach(void);
u8 WaitStop(u8 no);
void steptimeout_out(void);
void ReSetWarning(void);
void SetStopState(void);
unsigned char GetServoState(unsigned char no);
void Step_O_outtime(TIM_TypeDef *timers,u8 no);

void ResetStopState(void);
void SetLimitChange_p(unsigned char no,unsigned char changedno);
void SetLimitChange_n(unsigned char no,unsigned char changedno);

#endif 
