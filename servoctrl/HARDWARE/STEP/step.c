#include <math.h>
#include "step.h"
#include "timer.h"
#include "delay.h" 
#include "malloc.h"
#include "parameter.h"
#include "stepwarning.h"
#include "usart.h"
#include "servosys.h"
#include "cansys.h"
#include "debug.h"
//#define		SERO_IN_P(x)		PGin(x)
//#define		SERO_IN_N(x)		PGin((x+8))
unsigned char limit_changed_p[4];
unsigned char limit_changed_n[4];
unsigned short dropdata[4][5000];
struct _m_step m_step[4];
unsigned char timernum[4];
//char stepenter[4];
char steptimeout;
char stopstate[4];
char allstopstate;
//char stepwarning[4];
extern struct servo_state m_state;
extern struct parameter m_para;
char SERO_IN_P(char x)
{
	char ret=0;
	switch(x)
	{
		case 0:ret=PEin(4);break;
		case 1:ret=PDin(8);break;
		case 2:ret=PCin(12);break;
		case 3:ret=PBin(0);break;
		default:ret=0;break;
	}
	return ret;
}
char SERO_IN_N(char x)
{
	char ret=0;
	switch(x)
	{
		case 0:ret=PEin(5);break;
		case 1:ret=PDin(9);break;
		case 2:ret=PDin(0);break;
		case 3:ret=PBin(1);break;
		default:ret=0;break;
	}
	return ret;
}
void InitStep(void)
{
	int i;
	for(i=0;i<4;i++)
	{
		m_step[i].state=STOP_STATE;
		m_step[i].dropdata=NULL;
		m_step[i].stoptype=NORMAL_STOP;
		m_step[i].dir=DIR_P;
		m_step[i].dropdata=dropdata[i];
		m_state.stepstate[i]=0;
		limit_changed_p[i]=i;
		limit_changed_n[i]=i;
		timernum[i]=0;
		stopstate[i]=0;
		allstopstate=0;
//		stepenter[i]=0;
	//	stepwarning[i]=0;
		m_state.step_warning[i]=0;
	}
	m_step[0].inittimer=TIM2_Int_Init;
	m_step[1].inittimer=TIM3_Int_Init;
	m_step[2].inittimer=TIM4_Int_Init;
	m_step[3].inittimer=TIM5_Int_Init;
	
	m_step[0].o_speed_timer=TIM9_Int_Init;
	m_step[1].o_speed_timer=TIM10_Int_Init;
	m_step[2].o_speed_timer=TIM11_Int_Init;
	m_step[3].o_speed_timer=TIM12_Int_Init;
	
	steptimeout=0;
	
	RCC->AHB1ENR|=1<<0;// 使能 PORTA 时钟
	RCC->AHB1ENR|=1<<1;// 使能 PORTB 时钟
	RCC->AHB1ENR|=1<<2;// 使能 PORTC 时钟
	RCC->AHB1ENR|=1<<3;// 使能 PORTD 时钟
	RCC->AHB1ENR|=1<<4;// 使能 PORTE 时钟
//	RCC->AHB1ENR|=1<<5;// 使能 PORTF 时钟
	RCC->AHB1ENR|=1<<6;// 使能 PORTG 时钟
//	GPIO_Set(GPIOE,0xFFFF,GPIO_MODE_OUT,GPIO_OTYPE_PP,GPIO_SPEED_100M,GPIO_PUPD_PU);
	GPIO_Set(GPIOA,PIN4|PIN5|PIN6|PIN7,GPIO_MODE_OUT,GPIO_OTYPE_PP,GPIO_SPEED_100M,GPIO_PUPD_PU);
	GPIO_Set(GPIOB,PIN6|PIN7|PIN8|PIN9,GPIO_MODE_OUT,GPIO_OTYPE_PP,GPIO_SPEED_100M,GPIO_PUPD_PU);
//	GPIO_Set(GPIOC,PIN0|PIN1|PIN2|PIN3,GPIO_MODE_OUT,GPIO_OTYPE_PP,GPIO_SPEED_100M,GPIO_PUPD_PU);
//	GPIO_Set(GPIOF,PIN2|PIN3|PIN4|PIN5|PIN6|PIN7|PIN8|PIN9,GPIO_MODE_OUT,GPIO_OTYPE_PP,GPIO_SPEED_100M,GPIO_PUPD_PU);
//	GPIO_Set(GPIOF,PIN6,GPIO_MODE_IN,0,0,GPIO_PUPD_NONE);//设置FIN6为输入
	GPIO_Set(GPIOB,PIN0|PIN1,GPIO_MODE_IN,0,0,GPIO_PUPD_NONE);
	GPIO_Set(GPIOC,PIN12,GPIO_MODE_IN,0,0,GPIO_PUPD_NONE);
	GPIO_Set(GPIOD,PIN0|PIN8|PIN9,GPIO_MODE_IN,0,0,GPIO_PUPD_NONE);
	GPIO_Set(GPIOE,PIN4|PIN5,GPIO_MODE_IN,0,0,GPIO_PUPD_NONE);
//	GPIO_Set(GPIOF,PIN14|PIN15,GPIO_MODE_IN,0,0,GPIO_PUPD_NONE);
	GPIO_Set(GPIOG,0xFFFF,GPIO_MODE_IN,0,0,GPIO_PUPD_NONE);
//	GPIO_Set(GPIOG,PIN13|PIN14,GPIO_MODE_IN,0,0,GPIO_PUPD_NONE);
}
void SetLimitChange_p(unsigned char no,unsigned char changedno)
{
	limit_changed_p[no]=changedno;
}
void SetLimitChange_n(unsigned char no,unsigned char changedno)
{
	limit_changed_n[no]=changedno;
}
char GetLimitChangedState_p(char no)
{
	return SERO_IN_P(limit_changed_p[no]);
}
char GetLimitChangedState_n(char no)
{
	return SERO_IN_N(limit_changed_n[no]);
}
u8 TestingStepWarning(void)
{
	u8 ret=0;
	int i;
	for(i=0;i<4;i++)
	{
		if(m_state.step_warning[i]!=0)
		{
			ret =1;
			break;
		}
	}
	return ret;
}
u8 StarStep(u8 no,u16 speed,u32 distance,u16 dropnumber,u8 dir,u8 type)
{
	int i;
	float speedb;
	u16 *pdata;
	if(allstopstate==1)
	{
		printf("all stop state\r\n");
		return 1;
	}
	if(TestingStepWarning())
	{
		#if isdebug
				printf("TestingStepWarning\r\n");
		#endif
		return 1;
	}
	if(stopstate[no])
	{
		#if isdebug
				printf("step is not stop state\r\n");
		#endif
		return 0;
	}
//	printf("star step\r\n");
	if(m_step[no].state!=STOP_STATE)
	{
	//	printf("step is not stop\r\n");
		#if isdebug
				printf("step is not stop\r\n");
		#endif
		return 0;
	}
	if((GetPLimitState(no)!=0)&&(dir==DIR_P))
	{
//		printf("step limit P\r\n");
		#if isdebug
				printf("step limit P\r\n");
		#endif
		SendServoStopState(no);
		return 1;
	}
	if((GetNLimitState(no)!=0)&&(dir==DIR_N))
	{
	//	printf("step limit N\r\n");
			#if isdebug
				printf("step limit N\r\n");
				printf("m_para.m_step[no].directional_logic[1]=%d;SERO_IN_N(no)=%d\r\n",m_para.m_step[no].directional_logic[1],SERO_IN_N(no));
		#endif
		SendServoStopState(no);
		return 1;
	}
	
//	printf("step 1\r\n");
	m_step[no].dir=dir;
	m_step[no].curnumberplace=0;
	m_step[no].state=ASCENDANT_STATE;
	m_state.stepstate[no]=ASCENDANT_STATE;
	if(dropnumber>4096)
		dropnumber=4096;
	if(distance>2*dropnumber)
	{
		m_step[no].distance=distance-2*dropnumber;
//		m_step[no].distance=m_para.m_step[no].step_scaling;
		
	}
	else
	{
		
		dropnumber=distance/2;
		m_step[no].distance=distance-2*dropnumber;;
	}
		#if isdebug
				printf("distance=%d\r\n",m_step[no].distance);
		#endif
	m_step[no].type=type;
	
	m_step[no].dropnumber=dropnumber;
	STEP_DIR(no)=dir;
	
	speedb=1000000/(speed*(pow(dropnumber,1.0/3.0)-pow(dropnumber-1,1.0/3.0)));
	pdata=m_step[no].dropdata;
//	printf("drop data:\r\n");
	for(i=1;i<dropnumber+2;i++)
	{
		u16 stemp;
		float ptemp;
		if(i==1)
			ptemp=1.0;
		else
			ptemp=pow(i,1.0/3.0)-pow(i-1,1.0/3.0);
		stemp=(u16)(ptemp*speedb);
//		printf("0x%x:",stemp);
		*pdata=stemp;
		pdata++;
		
	}
	
//	printf("m_step[no].dir=%d,m_step[no].distance=%d,m_step[no].type=%d\r\n",m_step[no].dir,m_step[no].distance,m_step[no].type);
//	printf("\r\n");
	delay_us(10);
//	printf("%d,%d,%d\r\n",no,m_step[no].dropdata[0],m_para.m_step[no].step_timer_psc);
	m_step[no].inittimer(m_step[no].dropdata[0],step_timer_psc);
//	delay_us(100000);
//	printf("%d\r\n",m_para.step_o_timer);
//	m_step[no].o_speed_timer(m_para.step_o_timer,16799);
	#if isdebug
	 printf("timeout timer=%d\r\n",m_para.step_o_timer);
	#endif
	STEP_PLUG(no)=1;
//	if(no>3)
//	{
//		PCout((no-3))=!PCout((no-3));
//	}
//	else
//	{
//		PBout((no+10))=!PBout((no+10));
//	}
	return 1;
}
void Step_O_outtime(TIM_TypeDef *timers,u8 no)
{
	//	initliftererror();

	if(m_step[no].state!=0)
	{
//		printf("outimer \r\n");
		if(timernum[no]>=1)
		{
		
			if(step_o_s(no))
			{
//				printf("outimer fin\r\n");
				
				
				Stopall();
			}
			timers->CR1&=0xFE;//关闭定时器
			timernum[no]=0;
		}
		timernum[no]++;
	}
		
		timers->SR&=~(1<<0);//清除中断标志位 
	//			stepenter[no]=0;
	
}
void StepHand(TIM_TypeDef *timers,u8 no,volatile unsigned long*  place)
{
	if(*place==0)
	{
//		printf("*");
		if(stopstate[no]==0)
		{
			
			if(((((GetPLimitState(no)!=0)&&(m_step[no].dir==DIR_P))||((GetNLimitState(no))&&(m_step[no].dir==DIR_N)))||(m_state.stepstate[no]==0))||(testwarningstate()))
			{
				stopstate[no]=1;
				m_state.stepstate[no]=1;
				if(m_step[no].stoptype==NORMAL_STOP)
				{
					m_step[no].state=DROP_STATE;
					m_state.stepstate[no]=DROP_STATE;
				}
				else
				{
					m_step[0].state=STOP_STATE;
					
					*place=0;
					timers->CR1&=0xFE;//关闭定时器
					timers->SR&=~(1<<0);//清除中断标志位 
	//			stepenter[no]=0;
					m_state.stepstate[no]=0;
					stopstate[no]=0;
					SendServoStopState(no);
					return;
				}
			}
//			printf("?");
	//		stepenter[no]=1;
		
		}
			
		
		
	//		printf("&");
			switch(m_step[no].state)
			{
				case ASCENDANT_STATE:
					m_step[no].curnumberplace++;
					if(m_step[no].curnumberplace==m_step[no].dropnumber-1)
					{
						m_step[no].state=CONSTANT_STATE;
						m_state.stepstate[no]=CONSTANT_STATE;
						if((m_step[no].distance==0)&&(m_step[no].type==DISTANCE_TYPE))
						{
							m_step[no].state=DROP_STATE;
							m_state.stepstate[no]=DROP_STATE;
						}
						m_step[no].curdistance=0;
					}
					break;
				case CONSTANT_STATE:
					if(m_step[no].type==DISTANCE_TYPE)
					{
						m_step[no].curdistance++;
						if(m_step[no].curdistance==m_step[no].distance)
						{
							m_step[no].state=DROP_STATE;
							m_state.stepstate[no]=DROP_STATE;
							#if isdebug
								printf("curdistance=%d\r\n",m_step[no].curdistance);
							#endif
						}
					}
					break;
				case DROP_STATE:
					if(m_step[no].curnumberplace>0)
						m_step[no].curnumberplace--;
					else
						m_step[no].curnumberplace=0;
					if(m_step[no].curnumberplace==0)
					{
		//				printf(".");
						m_step[no].state=STOP_STATE;
						m_state.stepstate[no]=DROP_STATE;
						
						*place=0;
						stopstate[no]=0;
						timers->CR1&=0xFE;//关闭定时器
						timers->SR&=~(1<<0);//清除中断标志位 
	//					stepenter[no]=0;
						m_state.stepstate[no]=0;
						SendServoStopState(no);
						return;
					}
					break;
				default:
	//				printf("?");
					m_step[no].state=STOP_STATE;
				m_state.stepstate[no]=DROP_STATE;
					
					*place=0;
					timers->CR1&=0xFE;//关闭定时器
					timers->SR&=~(1<<0);//清除中断标志位 
//					stepenter[no]=0;
				m_state.stepstate[no]=0;
	//				printf("&");
						return;
					
		
			}
			
				
		
			
	}
//		STEP_PLUG(0)=!STEP_PLUG(0);
	timers->ARR=m_step[no].dropdata[m_step[no].curnumberplace];
	*place=!(*place);
}
void EnterDropState(u8 no)
{
	m_step[no].state=DROP_STATE;
	m_state.stepstate[no]=DROP_STATE;
}
void StopStep(u8 no)
{
	if(m_step[no].state!=STOP_STATE)
	{
		if(m_step[no].stoptype==EMERGENCY_STOP)
		{
//		if(no>3)
//		{
//			PCout((no-3))=0;
//		}
//		else
//		{
//			PBout((no+10))=0;
//		}
			STEP_PLUG(no)=0;
			m_step[no].state=STOP_STATE;
			m_state.stepstate[no]=DROP_STATE;
			
		}
		else
		{
			m_step[no].state=DROP_STATE;
			m_state.stepstate[no]=DROP_STATE;
		}
	}
}
void SetStopType(u8 no,u8 stoptype)
{
	m_step[no].stoptype=stoptype;
}
u8 StepDir(u8 no)
{
	return m_step[no].dir;
}

void StopStepNow(u8 no)
{
	m_step[no].state=STOP_STATE;
	m_state.stepstate[no]=DROP_STATE;
}
unsigned char GetServoState(unsigned char no)
{
	return m_step[no].state;
}
u8 GetPLimitState(u8 no)
{
	u8 ret;
	ret=0;
//	if(m_step[no].dir==DIR_P)
//	{
	
		if((m_para.m_step[no].directional_logic[0]==1)&&(SERO_IN_P(no)!=0))
		{
			ret=1;
		}
		if((m_para.m_step[no].directional_logic[0]==0)&&(SERO_IN_P(no)==0))
		{
			ret=1;
		}
		m_state.step_limit_p[no]=ret;
//	}
	return ret;
}
u8 GetNLimitState(u8 no)
{
	u8 ret;
	ret=0;
//	if(m_step[no].dir==DIR_N)
//	{
		if((m_para.m_step[no].directional_logic[1]==1)&&(SERO_IN_N(no)!=0))
		{
			ret=1;
			
		}
		if((m_para.m_step[no].directional_logic[1]==0)&&(SERO_IN_N(no)==0))
		{
			ret=1;
		}
		m_state.step_limit_n[no]=ret;
	//}
	return ret;
}
void LimitSeach(void)
{
	int i;
	for(i=0;i<4;i++)
	{
		if(m_step[i].state!=STOP_STATE)
		{
			if((GetPLimitState(i)!=0)||(GetNLimitState(i)!=0))
			{
				StopStep(i);
			}
		}
	}
}
void steptimeout_out(void)
{
	steptimeout=1;
}
void startimeout()
{
	steptimeout=0;
	printf("out time is %d\r\n",(u16)m_para.outtimerset);
	TIM13_Int_Init((u16)m_para.outtimerset,0xFFFF);
	
}
u8 WaitStop(u8 no)
{
	u8 ret=0;
	int i;
	
//	delay_ms(1);
	i=0;

	
	while(m_step[no].state!=STOP_STATE)
	{
		delay_ms(10);
		i++;
		if(i>=m_para.outtimerset)
		{
			StopStepNow(no);
			m_state.step_warning[no]=1;
	//		stepwarning[no]=1;
			ret=1;
			break;
		
		}
		
	}
	return ret;
			
			
	
}
void ReSetWarning(void)
{
	int i;
	printf("reset warning\r\n");
	for(i=0;i<4;i++)
	{
//		stepwarning[i]=0;
		m_state.step_warning[i]=0;
	}
}
void SetStopState(void)
{
	allstopstate=1;
}
void ResetStopState(void)
{
	allstopstate=0;
}
