#include "raster.h"
#include "sys.h" 
#include "delay.h"
#include "parameter.h"
unsigned int y_raster_val;
unsigned int next_y_pl;
extern struct parameter m_para;
void EXTI1_IRQHandler(void)
{
	delay_us(2);
	if(y_raster_in!=0)
	{
		y_raster_val++;
	}
	EXTI->PR=1<<1;  //清除LINE1上中断标志位
}
void EXTI3_IRQHandler(void)
{
	delay_us(20);
	if(y_pl!=0)
	{
		next_y_pl = (unsigned int)(y_raster_val/((double)m_para.y_raster_scaling/1000));
	}
	EXTI->PR=1<<3;  //LINE3
}
void InitRaster(void)
{
	RCC->AHB1ENR|=1<<6;     //PORTG
	RCC->AHB1ENR|=1<<2;     //PORTC
	y_raster_val=0;
	GPIO_Set(GPIOG,PIN1,GPIO_MODE_IN,0,0,GPIO_PUPD_PD);
	GPIO_Set(GPIOC,PIN3,GPIO_MODE_IN,0,0,GPIO_PUPD_PU);
	Ex_NVIC_Config(GPIO_G,1,RTIR);
	Ex_NVIC_Config(GPIO_C,3,RTIR);
	MY_NVIC_Init(0,3,EXTI1_IRQn,2);	
	MY_NVIC_Init(1,3,EXTI3_IRQn,2);
}

